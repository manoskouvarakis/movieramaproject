# MovieRama

The project is deployed in Azure. To see it please visit https://movierama.azurewebsites.net/

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes

### Prerequisites

In order to run the application locally, you will need [Microsoft Visual Studio 2017 Community](https://www.visualstudio.com/vs/community/) (or higher). 
The project uses Microsoft LocalDB. If you don't have it, please install [ SQL Server 2016 Express with LocalDB](https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-2016-express-localdb).
If you want to run it with Microsoft SQL Server a default connection (where you should change only server name) is commented out in appsettings.json

### Installing

In order to run the project in Visual Studio 2017:

```
- Open Package Manager Console (PMC)
- Execute command "Update-Database"
- Press F5 to run on ISS Express
```

### Running the tests

In order to run the tests in Visual Studio 2017:

```
- Test -> Run -> All Tests
```
