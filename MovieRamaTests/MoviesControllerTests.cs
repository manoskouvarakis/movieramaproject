﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using MovieRama.Controllers;
using MovieRama.DataAccess;
using MovieRama.Models;
using MovieRama.Models.ModelBuilder;
using MovieRama.Models.MoviesViewModels;
using MovieRama.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MovieRama.Tests.Controllers
{
	public class MoviesControllerTests
	{
		[Fact]
		public void Index_ReturnsViewResultWithMoviesList()
		{
			// Arrange
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(It.IsAny<MovieCriteria>())).Returns(GetTestMovies());

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);
			
			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			// Act
			var result = controller.Index(new Models.Query.MovieQueryModel());

			// Assert
			var viewResult = Assert.IsType<ViewResult>(result);
			var model = Assert.IsAssignableFrom<IEnumerable<MovieViewModel>>(viewResult.ViewData.Model);
			Assert.Equal(this.GetTestMovies().Count, model.Count());
		}

		[Fact]
		public void Details_ReturnsViewResultWithSpecificMovie()
		{
			//Arrange
			int movieId = 0;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Details(movieId);

			//Assert
			var viewResult = Assert.IsType<ViewResult>(result);
			var model = Assert.IsAssignableFrom<MovieViewModel>(viewResult.ViewData.Model);
			Assert.Equal(model.ID, movieId);
		}

		[Fact]
		public void Details_ReturnsHttpNotFound_ForInvalidMovie()
		{
			//Arrange
			int? movieId = null;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Details(movieId);

			//Assert
			Assert.IsType<NotFoundResult>(result);
		}

		[Fact]
		public void Details_ReturnsHttpNotFound_ForNotExistingMovie()
		{
			//Arrange
			int movieId = 123;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Details(movieId);

			//Assert
			Assert.IsType<NotFoundResult>(result);
		}

		[Fact]
		public void Create_RedirectsToIndexViewForSuccessCreation()
		{
			//Arrange
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Persist(It.IsAny<Movie>()));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Create(new MovieViewModel());

			//Assert
			var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
			Assert.Equal("Index", redirectToActionResult.ActionName);
		}

		[Fact]
		public void Create_RedirectsToViewForInvalidViewModel()
		{
			//Arrange
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Persist(It.IsAny<Movie>()));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);
			controller.ModelState.AddModelError("Description", "Required");

			//Act
			var result = controller.Create(new MovieViewModel());

			//Assert
			Assert.IsNotType<RedirectToActionResult>(result);
		}

		[Fact]
		public void Edit_ReturnsViewResultWithSpecificMovie()
		{
			//Arrange
			int movieId = 0;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Edit(movieId);

			//Assert
		    var viewResult = Assert.IsType<ViewResult>(result);
			var model = Assert.IsAssignableFrom<MovieViewModel>(viewResult.ViewData.Model);
			Assert.Equal(model.ID, movieId);
		}

		[Fact]
		public void DeleteConfirmed_RedirectsToIndexAfterSuccessDeletion()
		{
			//Arrange
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Delete(It.IsAny<int>()));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.DeleteConfirmed(It.IsAny<int>());

			//Assert
			var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
			Assert.Equal("Index", redirectToActionResult.ActionName);
			mockMoviekRepo.Verify(x => x.Delete(It.IsAny<int>()), Times.Exactly(1));
		}

		[Fact]
		public void Like_UserHasNotLikeOrHatedSpecificMovie()
		{
			//Arrange
			int movieId = 0;

			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			mockIImpressionRepository.Setup(repo => repo.Retrieve(It.IsAny<ImpressionCriteria>())).Returns(new List<Impression>());
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			mockIPolicyService.Setup(repo => repo.CanExpressimpression(It.IsAny<Movie>(), It.IsAny<String>())).Returns(true);
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Like(movieId);

			//Assert
			mockIImpressionRepository.Verify(x => x.Persist(It.IsAny<Impression>()), Times.Exactly(1));
			mockIImpressionRepository.Verify(x => x.Delete(It.IsAny<int>()), Times.Never);
		}

		[Fact]
		public void Like_UserHasLikedSpecificMovieAndShoudDeleteImpression()
		{
			//Arrange
			int movieId = 5;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			mockIImpressionRepository.Setup(repo => repo.Retrieve(It.IsAny<ImpressionCriteria>())).Returns(GetTestImpressions().Where(x => x.MovieID == movieId));
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			mockIPolicyService.Setup(repo => repo.CanExpressimpression(It.IsAny<Movie>(), It.IsAny<String>())).Returns(true);
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Like(movieId);

			//Assert
			mockIImpressionRepository.Verify(x => x.Persist(It.IsAny<Impression>()), Times.Never);
			mockIImpressionRepository.Verify(x => x.Delete(It.IsAny<int>()), Times.Exactly(1));
		}

		[Fact]
		public void Hate_UserHasNotHatedOrLikedSpecificMovie()
		{
			//Arrange
			int movieId = 0;

			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			mockIImpressionRepository.Setup(repo => repo.Retrieve(It.IsAny<ImpressionCriteria>())).Returns(new List<Impression>());
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			mockIPolicyService.Setup(repo => repo.CanExpressimpression(It.IsAny<Movie>(), It.IsAny<String>())).Returns(true);
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Hate(movieId);

			//Assert
			mockIImpressionRepository.Verify(x => x.Persist(It.IsAny<Impression>()), Times.Exactly(1));
			mockIImpressionRepository.Verify(x => x.Delete(It.IsAny<int>()), Times.Never);
		}

		[Fact]
		public void Hate_UserHasHatedSpecificMovieAndShoudDeleteImpression()
		{
			//Arrange
			int movieId = 6;
			var mockStringLocalizer = this.InitializeMockLocalizer();
			var mockILogger = new Mock<ILogger<MoviesController>>();

			var mockMoviekRepo = new Mock<IMovieRepository>();
			mockMoviekRepo.Setup(repo => repo.Retrieve(movieId)).Returns(GetTestMovies().FirstOrDefault(x => x.ID == movieId));

			var mockIImpressionRepository = new Mock<IImpressionRepository>();
			mockIImpressionRepository.Setup(repo => repo.Retrieve(It.IsAny<ImpressionCriteria>())).Returns(GetTestImpressions().Where(x => x.MovieID == movieId));
			var mockImpressionService = new Mock<ImpressionService>(mockIImpressionRepository.Object);

			var mockIPolicyService = new Mock<IPolicyService>();
			mockIPolicyService.Setup(repo => repo.CanExpressimpression(It.IsAny<Movie>(), It.IsAny<String>())).Returns(true);
			var mockHumanReadableDateTimeService = new Mock<HumanReadableDateTimeService>(mockStringLocalizer.Object);
			var mockMovieBuilder = new Mock<MovieBuilder>(mockIPolicyService.Object, mockHumanReadableDateTimeService.Object);

			var controller = new MoviesController(mockMoviekRepo.Object, mockImpressionService.Object, mockMovieBuilder.Object, mockIPolicyService.Object, mockILogger.Object);
			this.InitializeContextLoggedInUser(controller);

			//Act
			var result = controller.Hate(movieId);

			//Assert
			mockIImpressionRepository.Verify(x => x.Persist(It.IsAny<Impression>()), Times.Never);
			mockIImpressionRepository.Verify(x => x.Delete(It.IsAny<int>()), Times.Exactly(1));
		}

		#region Helpers

		private Mock<IStringLocalizer<SharedResources>> InitializeMockLocalizer()
		{
			var mockStringLocalizer = new Mock<IStringLocalizer<SharedResources>>();
			string key = "MovieRama Localized Key!";
			var localizedString = new LocalizedString(key, key);
			mockStringLocalizer.Setup(_ => _[key]).Returns(localizedString);
			return mockStringLocalizer;
		}

		private void InitializeContextLoggedInUser(MoviesController controller)
		{
			var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
			{
				 new Claim(ClaimTypes.NameIdentifier, "1"),
			}));
			controller.ControllerContext = new ControllerContext()
			{
				HttpContext = new DefaultHttpContext() { User = user }
			};
		}

		private List<Movie> GetTestMovies()
		{
			var movies = new List<Movie>();
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 0,
				Description = "Test1",
				Title = "Test1",
				UserID = "UserId1",
				User = new ApplicationUser() { UserName = "User1" },
				Impressions = new List<Impression>()
			});
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 1,
				Description = "Test2",
				Title = "Test2",
				UserID = "UserId2",
				User = new ApplicationUser() { UserName = "User2" },
				Impressions = new List<Impression>()
			});
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 2,
				Description = "Test3",
				Title = "Test3",
				UserID = "UserId3",
				User = new ApplicationUser() { UserName = "User3" },
				Impressions = new List<Impression>()
			});
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 3,
				Description = "Test4",
				Title = "Test4",
				UserID = "UserId4",
				User = new ApplicationUser() { UserName = "User4" },
				Impressions = new List<Impression>()
			});
			//Movie with like impression
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 5,
				Description = "MovieWithLike",
				Title = "Test5",
				UserID = "UserId5",
				User = new ApplicationUser() { UserName = "UserId5" },
				Impressions = new List<Impression>() {
					new Impression()
					{
						CreationTime = DateTime.UtcNow,
						ID = 0,
						Type = Impression.ImpressionType.Like,
						MovieID = 5,
						UserID = "1",
					} }
			});
			//Movie with hate impression
			movies.Add(new Movie()
			{
				CreationTime = DateTime.UtcNow,
				ID = 6,
				Description = "MovieWithHate",
				Title = "Test6",
				UserID = "UserId6",
				User = new ApplicationUser() { UserName = "UserId6" },
				Impressions = new List<Impression>() {
					new Impression()
					{
						CreationTime = DateTime.UtcNow,
						ID = 1,
						Type = Impression.ImpressionType.Hate,
						MovieID = 5,
						UserID = "1",
					} }
			});
			return movies;
		}

		private List<Impression> GetTestImpressions()
		{
			var impressions = new List<Impression>();
			impressions.Add(new Impression()
			{
				CreationTime = DateTime.UtcNow,
				ID = 0,
				Type = Impression.ImpressionType.Like,
				MovieID = 5,
				UserID = "1",
			});
			impressions.Add(new Impression()
			{
				CreationTime = DateTime.UtcNow,
				ID = 1,
				Type = Impression.ImpressionType.Hate,
				MovieID = 6,
				UserID = "1",
			});
			return impressions;
		}

		#endregion
	}
}
