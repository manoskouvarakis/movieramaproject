﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using MovieRama.DataAccess;
using MovieRama.Helpers;
using MovieRama.Models;
using MovieRama.Models.ModelBuilder;
using MovieRama.Models.MoviesViewModels;
using MovieRama.Models.Query;
using MovieRama.Services;

namespace MovieRama.Controllers
{
	public class MoviesController : Controller
	{
		private readonly IMovieRepository _movieRepository;
		private readonly ImpressionService  _impressionService;
		private readonly MovieBuilder _movieBuilder;
		private readonly IPolicyService _policyService;
		private readonly ILogger<MoviesController> _logger;

		public MoviesController(IMovieRepository movieRepository, ImpressionService impressionService, MovieBuilder movieBuilder, IPolicyService policyService, ILogger<MoviesController> logger)
		{
			_movieRepository = movieRepository;
			_impressionService = impressionService;
			_movieBuilder = movieBuilder;
			_policyService = policyService;
			_logger = logger;
		}

		// GET: Movies
		[HttpGet]
		public IActionResult Index(MovieQueryModel query)
		{
			_logger.LogInformation("Getting items");

			query.HandleQueryParameters();

			this.UpdateViewData(query);

			MovieCriteria criteria = query.BuildCriteria();

			IEnumerable<Movie> movies = _movieRepository.Retrieve(criteria);

			IEnumerable<MovieViewModel> viewModels = this._movieBuilder.Build(movies, this.User);

			return View(viewModels);
		}

		// GET: Movies/Details/5
		[HttpGet("{id}")]
		public IActionResult Details(int? id)
		{
			if (id == null)
			{
				_logger.LogError("Details({ID}) NOT FOUND", id);
				return NotFound();
			}

			var movie = this._movieRepository.Retrieve(id.Value);

			if (movie == null)
			{
				_logger.LogError("Movie({ID}) NOT FOUND", id);
				return NotFound();
			}

			return View(this._movieBuilder.Build(movie, this.User));
		}

		// GET: Movies/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: Movies/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize]
		public IActionResult Create([Bind("ID, Title, Description")] MovieViewModel movieViewModel)
		{
			if (ModelState.IsValid)
			{
				Movie movie = movieViewModel.ToDataModel(this.User);
				_movieRepository.Persist(movie);

				return RedirectToAction(nameof(Index));
			}
			_logger.LogError("Model State invalid");
			return View(movieViewModel);
		}

		// GET: Movies/Edit/5
		[Authorize]
		public IActionResult Edit(int? id)
		{
			if (id == null)
			{
				_logger.LogError("Edit({ID}) NOT FOUND", id);
				return NotFound();
			}

			var movie = _movieRepository.Retrieve(id.Value);

			if (movie == null)
			{
				_logger.LogError("Movie({ID}) NOT FOUND", id.Value);
				return NotFound();
			}

			return View(this._movieBuilder.Build(movie, this.User));
		}

		// POST: Movies/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize]
		public IActionResult Edit(int id, [Bind("ID, Title, Description, IsStored, LastUpdateTimeTicks")] MovieViewModel movieViewModel)
		{
			if (id != movieViewModel.ID)
			{
				_logger.LogError("Movie({ID}) NOT FOUND", id);
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					Movie existingMovie = this._movieRepository.Retrieve(id);
					if (this._policyService.HasChanged(existingMovie, movieViewModel)) return BadRequest();
					if (!this._policyService.CanEdit(existingMovie, this.User.GetUserId())) return Forbid();

					Movie movie = movieViewModel.ToDataModel(this.User, existingMovie);
					_movieRepository.Persist(movie);
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!this._movieRepository.Exists(movieViewModel.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}

			_logger.LogError("Model State invalid");
			return View(movieViewModel);
		}

		// GET: Movies/Delete/5
		[Authorize]
		public IActionResult Delete(int? id)
		{
			if (id == null)
			{
				_logger.LogError("Delete({ID}) NOT FOUND", id);
				return NotFound();
			}

			var movie = this._movieRepository.Retrieve(id.Value);

			if (movie == null)
			{
				_logger.LogError("Movie({ID}) NOT FOUND", id.Value);
				return NotFound();
			}

			return View(this._movieBuilder.Build(movie, this.User));
		}

		// POST: Movies/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize]
		public IActionResult DeleteConfirmed(int id)
		{
			_movieRepository.Delete(id);
			return RedirectToAction(nameof(Index));
		}

		[HttpPost, ActionName("Like")]
		[ValidateAntiForgeryToken]
		[Authorize]
		public IActionResult Like([FromBody] int? id)
		{
			if (id == null)
			{
				_logger.LogError("Like({ID}) NOT FOUND", id);
				return NotFound();
			}

			var movie = _movieRepository.Retrieve(id.Value);

			if (!_policyService.CanExpressimpression(movie, this.User.GetUserId()))
			{
				_logger.LogError("User {ID} forbiden to express impression", this.User.GetUserId());
				return Forbid();
			}

			Impression existing = this._impressionService.RetrieveExistingImpression(this.User.GetUserId(), movie.ID);

			this._impressionService.UpdateStoreImpression(existing, movie.ID, Impression.ImpressionType.Like, this.User);

			return PartialView("~/Views/Movies/_ImpressionPartial.cshtml", this._movieBuilder.Build(movie, this.User));
		}

		[HttpPost, ActionName("Hate")]
		[ValidateAntiForgeryToken]
		[Authorize]
		public IActionResult Hate([FromBody] int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var movie = _movieRepository.Retrieve(id.Value);

			if (!_policyService.CanExpressimpression(movie, this.User.GetUserId()))
			{
				_logger.LogError("User {ID} forbiden to express impression", this.User.GetUserId());
				return Forbid();
			}

			Impression existing = this._impressionService.RetrieveExistingImpression(this.User.GetUserId(), movie.ID);

			this._impressionService.UpdateStoreImpression(existing, movie.ID, Impression.ImpressionType.Hate, this.User);

			return PartialView("~/Views/Movies/_ImpressionPartial.cshtml", this._movieBuilder.Build(movie, this.User));
		}

		#region Helpers

		private void UpdateViewData(MovieQueryModel query)
		{
			ViewData["UserId"] = query.UserID;
			ViewData["OrderParam"] = query.OrderingParameterToString();
			ViewData["OrderDir"] = query.OrderingDirectionToString();
			ViewData["SearchString"] = query.SearchString;
		}

		#endregion
	}
}
