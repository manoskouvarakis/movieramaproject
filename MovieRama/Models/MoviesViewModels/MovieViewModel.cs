﻿using MovieRama.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Threading;

namespace MovieRama.Models.MoviesViewModels
{
	public class MovieViewModel : IViewModel
	{
		public int ID { get; set; }

		[Required(ErrorMessage = "The Title field is required."), MaxLength(50)]
		public string Title { get; set; }
		[Required(ErrorMessage = "The Description field is required."), MaxLength(450)]
		public string Description { get; set; }

		public string UserID { get; set; }
		public string UserName { get; set; }

		public DateTime CreationTime { get; set; }
		public String CreationTimeString { get; set; } //to have the culture for presentation

		public DateTime LastUpdateTime { get; set; }
		public long LastUpdateTimeTicks { get; set; }

		public int Likes { get; set; }
		public int Hates { get; set; }

		//True if logged in user has liked the movie 
		public bool HasLiked { get; set; }
		//True if logged in user has hated the movie
		public bool HasHated { get; set; }

		public bool CanExpressImpression { get; set; }
		public bool CanEdit { get; set; }

		public String HumanReadableCreationTime { get; set; }

		public bool IsStored { get; set; }

		public Movie ToDataModel(IPrincipal principal, Movie dataItem = null)
		{
			String userId = principal.GetUserId();
			if (this.IsStored && dataItem == null) throw new ArgumentException("stored item need to exist in db");

			if (dataItem == null) dataItem = new Movie();

			dataItem.ID = this.ID;
			dataItem.Title = this.Title;
			dataItem.Description = this.Description;
			dataItem.IsStored = this.IsStored;
			dataItem.UserID = userId;
			dataItem.CreationTime = this.IsStored ? dataItem.CreationTime : DateTime.UtcNow;
			dataItem.LastUpdateTime = DateTime.UtcNow;

			return dataItem;
		}
	}
}
