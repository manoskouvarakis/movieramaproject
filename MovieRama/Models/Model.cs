﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Models
{
	public class Model
	{
		[NotMapped]
		public bool IsStored { get; set; }
	}
}
