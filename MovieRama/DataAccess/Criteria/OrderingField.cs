﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
	public enum OrderType
	{
		Ascending = 0,
		Descending = 1
	}

	public class OrderingField<T>
	{
		public OrderType OrderType { get; set; }
		public Expression<Func<T, object>> EntityField { get; set; }
	}
}
