﻿using Microsoft.EntityFrameworkCore;
using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
	public class MovieCriteria : EntityCriteriaBase<Movie>
	{
		public IEnumerable<int> IDs { get; set; }
		public String UserId { get; set; } 
		public String Search { get; set; }

		public override IQueryable<Movie> Collect(DbSet<Movie> dbSet)
		{
			IQueryable<Movie> result = dbSet;
			if (this.IDs != null && this.IDs.Any())
				result = result.Where(x => this.IDs.Contains(x.ID));
			if (!String.IsNullOrEmpty(this.UserId))
				result = result.Where(x => x.UserID.Equals(this.UserId));

			result = result.Include(x => x.User);
			result = result.Include(x => x.Impressions);

			if (!String.IsNullOrEmpty(this.Search))
			{
				result = result.Where(x => x.User.UserName.Contains(this.Search) || x.Title.Contains(this.Search));
			}

			return this.ApplyLimits(this.ApplyOrdering(result));
		}
	}
}
