﻿using Microsoft.EntityFrameworkCore;
using MovieRama.Data;
using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
	public class ImpressionRepository : IImpressionRepository
	{
		private ApplicationDbContext Context;
		private DbSet<Impression> ImpressionEntity;

		public ImpressionRepository(ApplicationDbContext context)
		{
			this.Context = context;
			ImpressionEntity = context.Set<Impression>();
		}

		public void Delete(int id)
		{
			Impression m = this.Retrieve(id);
			if (m == null) return;

			this.Context.Remove(m);
			this.Context.SaveChanges();
		}

		public void Persist(Impression impression)
		{
			if (impression.IsStored)
				this.Context.Update(impression);
			else
			{
				this.Context.Add(impression);
			}
			this.Context.SaveChanges();
		}

		public Impression Retrieve(int id)
		{
			return this.Retrieve(new ImpressionCriteria() { IDs = new int[] { id } }).SingleOrDefault();
		}

		public IEnumerable<Impression> RetrieveAll()
		{
			return this.Retrieve(null);
		}

		public IEnumerable<Impression> Retrieve(ImpressionCriteria criteria)
		{
			IEnumerable<Impression> items = criteria == null ? this.ImpressionEntity.Include(x => x.User).Include(x => x.Movie).AsEnumerable() : criteria.Collect(this.ImpressionEntity).Include(x => x.User).Include(x => x.Movie).AsEnumerable();
			foreach (Impression item in items)
			{
				item.IsStored = true;
			}
				
			return items;
		}

		public bool Exists(int id)
		{
			return this.Context.Impression.Any(e => e.ID == id);
		}
	}
}
