﻿using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.DataAccess
{
    public interface IImpressionRepository
    {
		Impression Retrieve(int id);
		IEnumerable<Impression> RetrieveAll();
		IEnumerable<Impression> Retrieve(ImpressionCriteria criteria);
		void Persist(Impression impression);
		void Delete(int id);
		bool Exists(int id);
	}
}
