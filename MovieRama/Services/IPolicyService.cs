﻿using MovieRama.Models;
using MovieRama.Models.MoviesViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Services
{
    public interface IPolicyService
    {
		Boolean CanExpressimpression(Movie model, String currentUser);

		Boolean CanEdit(Movie model, String currentUser);

		Boolean HasChanged(Movie existing, MovieViewModel viewModel);
	}
}
