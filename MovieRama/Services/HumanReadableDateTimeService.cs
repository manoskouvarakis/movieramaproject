﻿using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Services
{
    public class HumanReadableDateTimeService
    {
		private readonly IStringLocalizer<SharedResources> _stringLocalizer;

		private Dictionary<HumanReadableDateTimeService.IntervalType, String> mTimeStrings = null;
		private Dictionary<HumanReadableDateTimeService.IntervalType, String> TimeStrings
		{
			get
			{
				if (this.mTimeStrings == null)
				{
					this.mTimeStrings = new Dictionary<HumanReadableDateTimeService.IntervalType, String>();
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Seconds, "about a second ago");
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Minutes, "about a minute ago");
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Hours, "about an hour ago");
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Days, "about a day ago");
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Months, "about a month ago");
					this.mTimeStrings.Add(HumanReadableDateTimeService.IntervalType.Years, "about a year ago");
				}
				return this.mTimeStrings;
			}
		}

		private Dictionary<HumanReadableDateTimeService.IntervalType, String> mTimePluralizedStrings = null;
		private Dictionary<HumanReadableDateTimeService.IntervalType, String> TimePluralizedStrings
		{
			get
			{
				if (this.mTimePluralizedStrings == null)
				{
					this.mTimePluralizedStrings = new Dictionary<HumanReadableDateTimeService.IntervalType, String>();
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Seconds, "{0} seconds ago");
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Minutes, "{0} minutes ago");
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Hours, "{0} hours ago");
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Days, "{0} days ago");
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Months, "{0} months ago");
					this.mTimePluralizedStrings.Add(HumanReadableDateTimeService.IntervalType.Years, "{0} years ago");
				}
				return this.mTimePluralizedStrings;
			}
		}

		public HumanReadableDateTimeService(IStringLocalizer<SharedResources> stringLocalizer)
		{
			_stringLocalizer = stringLocalizer;
		}

		public static readonly TimeSpan DurationMinute = TimeSpan.FromSeconds(60);
		public static readonly TimeSpan DurationYour = TimeSpan.FromMinutes(60);
		public static readonly TimeSpan DurationDay = TimeSpan.FromHours(24);
		public static readonly TimeSpan DurationMonth = TimeSpan.FromDays(30);
		public static readonly TimeSpan DurationYear = TimeSpan.FromDays(365);

		public enum IntervalType
		{
			Seconds,
			Minutes,
			Hours,
			Days,
			Months,
			Years
		}

		public String ToHumanReadableString(DateTime dateTime)
		{
			String humanReadableString = String.Empty;
			IntervalType interval;
			int time = 0;

			var timeSpan = DateTime.UtcNow.Subtract(dateTime);

			if (timeSpan <= HumanReadableDateTimeService.DurationMinute)
			{
				interval = IntervalType.Seconds;
				time = timeSpan.Seconds;
			}
			else if (timeSpan <= HumanReadableDateTimeService.DurationYour)
			{
				interval = IntervalType.Minutes;
				time = timeSpan.Minutes;
			}
			else if (timeSpan <= HumanReadableDateTimeService.DurationDay)
			{
				interval = IntervalType.Hours;
				time = timeSpan.Hours;
			}
			else if (timeSpan <= HumanReadableDateTimeService.DurationMonth)
			{
				interval = IntervalType.Days;
				time = timeSpan.Days;
			}
			else if (timeSpan <= HumanReadableDateTimeService.DurationYear)
			{
				interval = IntervalType.Months;
				time = timeSpan.Days / 30;
			}
			else
			{
				interval = IntervalType.Years;
				time = timeSpan.Days / 365;
			}

			humanReadableString = time > 1 ? _stringLocalizer[this.TimePluralizedStrings[interval], time] : _stringLocalizer[this.TimeStrings[interval]];

			return humanReadableString;
		}
	}
}
