﻿using MovieRama.DataAccess;
using MovieRama.Helpers;
using MovieRama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MovieRama.Services
{
    public class ImpressionService
    {
		private readonly IImpressionRepository _impressionRepository;

		public ImpressionService(IImpressionRepository impressionRepository)
		{
			this._impressionRepository = impressionRepository;
		}

		public Impression RetrieveExistingImpression(String userId, int movieId)
		{
			ImpressionCriteria criteria = new ImpressionCriteria
			{
				UserId = userId,
				MovieId = movieId
			};
			Impression impression = _impressionRepository.Retrieve(criteria).SingleOrDefault();

			return impression;
		}

		public void UpdateStoreImpression(Impression source, int movieId, Impression.ImpressionType userAction, IPrincipal principal)
		{
			//If impression in new, create the item and store it
			if (source == null)
			{
				source = this.CreateImpression(movieId, userAction, principal);
				_impressionRepository.Persist(source);
			}
			else
			{
				//If user has already expressed the same impression, remove it
				if (source.Type == userAction)
					_impressionRepository.Delete(source.ID);
				else //If user has expressed different impression, update it
				{
					source.Type = userAction;
					_impressionRepository.Persist(source);
				}
			}
		}

		private Impression CreateImpression(int movieId, Impression.ImpressionType impressionType, IPrincipal principal)
		{
			String userId = principal.GetUserId();

			Impression impression = new Impression
			{
				CreationTime = DateTime.UtcNow,
				UserID = userId,
				MovieID = movieId,
				Type = impressionType,
				IsStored = false
			};
			return impression;
		}
	}
}
