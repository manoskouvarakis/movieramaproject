﻿using MovieRama.Models;
using MovieRama.Models.MoviesViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieRama.Services
{
    public class PolicyService : IPolicyService
	{
		public Boolean CanExpressimpression(Movie model, String currentUser)
		{
			return !model.UserID.Equals(currentUser);
		}

		public Boolean CanEdit(Movie model, String currentUser)
		{
			return model.UserID.Equals(currentUser);
		}

		public Boolean HasChanged(Movie existing, MovieViewModel viewModel)
		{
			return !existing.LastUpdateTime.Ticks.Equals(viewModel.LastUpdateTimeTicks);
		}
	}
}
