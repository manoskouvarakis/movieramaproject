﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MovieRama.Models;

namespace MovieRama.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

			builder.Entity<Movie>().ToTable("Movie");
			builder.Entity<Impression>().ToTable("Impression");
		}

		public DbSet<MovieRama.Models.Movie> Movie { get; set; }

		public DbSet<MovieRama.Models.Impression> Impression { get; set; }
	}
}
