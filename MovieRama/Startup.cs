﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieRama.Data;
using MovieRama.Models;
using MovieRama.DataAccess;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using MovieRama.Models.ModelBuilder;
using MovieRama.Services;

namespace MovieRama
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

			services.Configure<IdentityOptions>(options =>
			{
				// Password settings
				options.Password.RequireDigit = true;
				options.Password.RequiredLength = 6;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = true;
				options.Password.RequireLowercase = false;

				// Lockout settings
				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
				options.Lockout.MaxFailedAccessAttempts = 10;
				options.Lockout.AllowedForNewUsers = true;

				// User settings
				options.User.RequireUniqueEmail = true;
			});

			services.ConfigureApplicationCookie(options =>
			{
				// Cookie settings
				options.Cookie.HttpOnly = true;
				options.Cookie.Expiration = TimeSpan.FromDays(150);
				options.LoginPath = "/Account/Login"; // If the LoginPath is not set here, ASP.NET Core will default to /Account/Login
				options.LogoutPath = "/Account/Logout"; // If the LogoutPath is not set here, ASP.NET Core will default to /Account/Logout
				options.AccessDeniedPath = "/Account/AccessDenied"; // If the AccessDeniedPath is not set here, ASP.NET Core will default to /Account/AccessDenied
				options.SlidingExpiration = true;
			});

			services.AddMvc()
				 .AddViewLocalization(
					 LanguageViewLocationExpanderFormat.Suffix,
					 opts => { opts.ResourcesPath = "Resources"; })
				 .AddDataAnnotationsLocalization(options => {
					 options.DataAnnotationLocalizerProvider = (type, factory) =>
						 factory.Create(typeof(SharedResources));
				 });

			services.Configure<RequestLocalizationOptions>(
			   opts =>
			   {
				   var supportedCultures = new List<CultureInfo>
					{
						new CultureInfo("en"),
						new CultureInfo("fr"),
					};

				   opts.DefaultRequestCulture = new RequestCulture("en");
				   // Formatting numbers, dates, etc.
				   opts.SupportedCultures = supportedCultures;
				   // UI strings that we have localized.
				   opts.SupportedUICultures = supportedCultures;
			   });

			services.Configure<MvcOptions>(options =>
			{
				options.Filters.Add(new RequireHttpsAttribute());
			});

			services.AddScoped<IMovieRepository, MovieRepository>();
			services.AddScoped<IImpressionRepository, ImpressionRepository>();

			services.AddTransient<IPolicyService, PolicyService>();
			services.AddTransient<MovieBuilder>();
			services.AddTransient<HumanReadableDateTimeService>();
			services.AddTransient<ImpressionService>();
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
			}
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

			app.UseStatusCodePagesWithRedirects("/Home/Error/{0}");

			var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
			app.UseRequestLocalization(options.Value);

			app.UseStaticFiles();

            app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Movies}/{action=Index}/{id?}");
			});

			var rewriterOptions = new RewriteOptions().AddRedirectToHttps();
			app.UseRewriter(rewriterOptions);
		}
    }
}
